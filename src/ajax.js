///api.ancraft.one

//get() //首次執行

function get() { //ajax get api
  s = $("#s").val();//取得keyword
  const api = 'http://127.0.0.1:19132/s/' + s;//api請求地址
  document.title = s + "-的搜尋結果"; //設定標題
  // $.ajax({
  //   type: "GET",
  //   headers: {},
  //   url: api,
  //   dataType: "json",
  //   crossDomain : true,
  //   beforeSend: function(){
  //     // request api.ancraft.one
  //   },
  //   success: function (response) { //api請求成功
  //     const jsond = []; //資料總表
  //     jsond.push(...response); //儲存json到列表中
  //     lodata(jsond); //開始處理資料
  //   },
  //   error: function (thrownError) {
  //     console.log(thrownError); //列印錯誤訊息
  //     template() //模擬展示
  //   }
  // });
  jsond = JSON.parse(localStorage.getItem(s)); //儲存json到列表中
  lodata(jsond); //開始處理資料


  function lodata(jsond) { //處理數據
    
    
    // console.log(jsond);
    var name = [], standard_deviation = []; //分類資料
    var price = [], by = [], ann = [],sho = 0,store = []; //分類資料
  
    for (var i=0; i<jsond.length; i++) { //循環處理每列資料
      row = jsond[i]; //正在處理列
      name.push( row['type'] );
      price.push( row['price'] );
      by.push( row['date'] );
      ann.push( row['annotation'] );
      sho += parseInt(row['price'])
      // console.log(row)
    }
    stats(name,price,by,ann,store,sho,jsond); //統計
    makePlot( name, price, by, ann, store, sho, standard_deviation, jsond ); //製作圖表
  }
  
  
  
  function stats(name,price,by,img,store,sho,jsond){ //製作數據統計
    price.forEach(function(v,i,a){
      a[i] = v.replace(/,/g, ""); //去除逗號
    });
  
    var data = {
      "data_1":"000",
      "data_2":"000",
      "data_3":"000",
      "data_4":"000",
      "data_5":"000"
    };
    anime({ //統計數字自動增加動畫
      targets: data,
      data_1: sho,
      data_2: Math.max(...price),
      data_3: Math.max(...price)-Math.min(...price),
      data_4: Math.min(...price),
      data_5: jsond.length,
      round: 1,
      easing: 'easeInOutExpo',
      delay: 300,
      update: function(){
        $("#data_1").html(data['data_1']);
        $("#data_2").html(data['data_2']);
        $("#data_3").html(data['data_3']);
        $("#data_4").html(data['data_4']);
        $("#data_5").html(data['data_5']);
      }
    });
  
    $("#data_1").text(jsond.length); //搜尋到的數量
    $("#data_2").text(Math.max(...price)); 
    $("#data_3").text(Math.max(...price)-Math.min(...price))
    $("#data_4").text(Math.min(...price));
    $("#data_5").text(jsond.length);
  };//statsend 統計結束
  
  function template() { //製作展示模型
    var data = {
      "data_1":"000",
      "data_2":"000",
      "data_3":"000",
      "data_4":"000",
      "data_5":"000"
    };
    anime({ //統計數字自動增加動畫
      targets: data,
      data_1: Math.random() * 10000,
      data_2: Math.random() * 10000,
      data_3: Math.random() * 10000,
      data_4: Math.random() * 10000,
      data_5: Math.random() * 10000,
      round: 1,
      easing: 'easeInOutExpo',
      delay: 131,
      update: function(){
        $("#data_1").html(data['data_1']);
        $("#data_2").html(data['data_2']);
        $("#data_3").html(data['data_3']);
        $("#data_4").html(data['data_4']);
        $("#data_5").html(data['data_5']);
      }
    });
  }
  
  function makePlot(name, price, by, ann, store, sho, standard_deviation, jsond ){ //製作圖形化資料表
      var layout= {
        plot_bgcolor:"rgb(42 44 49)",
        paper_bgcolor:"rgb(42 44 49)",
        font: {
          color: 'white'
        },
      }
      var alllayout= {
        plot_bgcolor:"rgb(42 44 49)",
        paper_bgcolor:"rgb(42 44 49)",
        font: {
          color: 'white'
        },
        xaxis: {
          rangeslider: {range: [by[1], by[-1]]},
          type: 'date'
        },
      }
      var playout= {
        plot_bgcolor:"rgb(42 44 49)",
        paper_bgcolor:"rgb(42 44 49)",
        font: {
          color: 'white'
        },
        xaxis: {
          rangeslider: {range: [by[1], by[-1]]},
          type: 'date'
        },
        showlegend: false,
      }
    //商品價格
      var traces = [{
        labels: by, 
        vaules: price.length,
        type: 'pie',
        marker: {
        color: 'rgb(142,124,195)'
        },
        automargin: false
      }];
      Plotly.newPlot('prices', traces, playout, 
        {title: '商品價格(無排序)'});
    
        
    //當日消費ˇ金額
      var traces = [{
        x: by, 
        y: price,
        type: 'bar',
        marker: {
        color: 'rgb(240 236 255)'
        },
        transforms: [{
          type: 'aggregate',
          groups: by,
          aggregations: [
            {target: 'y', func: 'sum', enabled: true},
          ]
        }]
      }];
      Plotly.newPlot('all', traces, alllayout, 
        {title: '當日消費金額'});
    

    //最高類型
    var traces = [{
      x: name, 
      y: price,
      type: 'bar',
      marker: {
      color: 'rgb(142,124,195)'
      },
      transforms: [{
        type: 'aggregate',
        groups: name,
        aggregations: [
          {target: 'y', func: 'sum', enabled: true},
        ]
      }]
    }];
    Plotly.newPlot('3bo', traces, layout, 
      {title: '最高類型'});
  

    //廠商占比
      var traces = [{
        labels: _.compact(name), //處理錯誤或空值
        type: 'pie',
        automargin: false
        }];
        Plotly.newPlot('pie', traces, layout, 
          {title: '花費占比'});
    
    
    
    //零售商店
    var traces = [{
      type: 'table', 
      columnwidth: [150,200,200,150],
      columnorder: [0,1,2,3],
      marker: {
        color: 'rgb(142,124,195)'
      },
      header: {
        values: ["日期","類型","價格","備註"],
        align: "center",
        line: {width: 1, color: 'rgb(50, 50, 50)'},
        fill: {color: ['rgb(142 124 195)']},
        font: {family: "Arial", size: 11, color: "white"}
      },
      cells: {
        values: [by,name,price,ann],
        align: ["center", "center"],
        line: {color: "black", width: 1},
        fill: {color: ['rgb(235, 193, 238)', 'rgba(228, 222, 249, 0.65)']},
        font: {family: "Arial", size: 10, color: ["black"]}
      },
      }];
      Plotly.newPlot('bysanc', traces, layout, 
        {title: ''});
    

      //雷達圖
      var traces = [{
        value: sho, 
        type: 'indicator',
        marker: {
        color: 'rgb(142,124,195)'
        },
        mode: "gauge+number",
      }];
      Plotly.newPlot('anc', traces, layout, 
        {title: '雷達圖'});
    
    
    
    };//plotend
  
  };//ajax get api請求結束
  
  
  function sidebar() { //側邊導覽切換
    $("body").toggleClass("opennav");
  };
  
  
  $(document).ready(function(){ //按下enter送出請求
    $("input").keyup(function(id){
      if(id.key === "Enter"){get()};//請求api
      });
  });
